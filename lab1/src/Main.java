//import com.sun.deploy.util.SystemUtils;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Scanner;

public class Main
{
	private static String readUsingScanner(String fileName) throws IOException
	{
		Path path = Paths.get(fileName);
		Scanner scanner = new Scanner(path);

		String content = "";

		while(scanner.hasNextLine())
		{
			content += scanner.nextLine();
		}

		return content;
	}

	public static void main(String args[])
	{
		// # Лабораторная работа №1. Коллекции.
		// Исходные данные следует считывать из текстового файла (формат записей произвольный). Результаты выводить на консоль.

		// 10. Задан файл с текстом на английском языке.
		//     Выделить все различные слова.
		//     Слова, отличающиеся только регистром букв, считать одинаковыми.
		//     Использовать класс HashSet.

		String content = null;
		try
		{
			content = readUsingScanner("text.txt");
		}
		catch (IOException e)
		{
			System.out.println("Can't read the file.");
			return;
		}

		String[] words = content
			.replaceAll("[\\.\\,\"\'-]", " ")
			.replace("  ", " ")
			.split(" ");


		System.out.println("Source text:\n" + content + "\n\n");

		HashSet<String> uniqueWords = new HashSet<>();

		for (String word : words)
		{
			uniqueWords.add(word.toLowerCase());
		}

		System.out.println("Unique words:");

		for (String word : uniqueWords)
		{
			System.out.println(word);
		}
	}
}
