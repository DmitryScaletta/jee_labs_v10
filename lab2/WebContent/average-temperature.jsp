<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.io.File" %>
<%@ page import="java.io.BufferedReader" %>
<%@ page import="java.io.IOException" %>
<%@ page import="java.nio.file.Files" %>
<%@ page import="java.nio.file.Paths" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.stream.Collectors" %>
<%
  String jspPath = getServletContext().getRealPath("/");
  String fileName = jspPath + java.io.File.separator + "temperatures.txt";
  List<Integer> list = new ArrayList<>();

  try (BufferedReader br = Files.newBufferedReader(Paths.get(fileName)))
  {
    list = br.lines().map(Integer::parseInt).collect(Collectors.toList());
  }
  catch (IOException e)
  {
    e.printStackTrace();
  }

  Double  averageTemperature    = list.stream().mapToDouble(val -> val).average().getAsDouble();
  Integer daysHigherThanAverage = list.stream().filter(t -> t > averageTemperature).toArray().length;
%>
<!DOCTYPE html>
<html lang="ru">
<head>
  <title></title>
  <meta charset="UTF-8">
  <link rel="stylesheet" href="style.css">
</head>
<body>
  <div class="button-back">
    <a href="./">&larr; Назад</a>
  </div>
  <div class="title">4. Взаимодействие HTML/JSP-сервлет</div>
  <div class="desc">
    8) Информация о среднесуточной температуре воздуха за месяц задана в виде списка, хранящегося в файле. Определить: 
    <br>
    &emsp;б) количество дней, когда температура была выше среднемесячной;
  </div>

  <div>
    <span class="title">Температура по дням</span>
    <table>
      <% for (int i = 0; i < 2; ++i) { %>
        <tr>
          <th>
              <%= (i == 0) ? "День" : "Температура" %>
          </th>
          <% for (int j = 0; j < list.size(); ++j) { %>
            <td class="<%= (i == 1 && list.get(j) > averageTemperature) ? "red" : "" %>">
              <%= (i == 0) ? j + 1 : list.get(j) %>
            </td>
          <% } %>
        </tr>
      <% } %>
    </table>
  </div>

  <br>

  <table>
    <tr>
      <th>Среднемесячная температура</th>
      <td>
        <%= averageTemperature %>
      </td>
    </tr>
    <tr>
      <th>Количество дней, когда температура была выше среднемесячной</th>
      <td>
        <%= daysHigherThanAverage %>
      </td>
    </tr>
  </table>

</body>
</html>