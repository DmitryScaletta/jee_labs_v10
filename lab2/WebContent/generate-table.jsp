<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
  String colsParam    = request.getParameter("cols");
  String rowsParam    = request.getParameter("rows");
  String titleParam   = request.getParameter("title");
  String bgColorParam = request.getParameter("bg-color");
  Integer cols = 0;
  Integer rows = 0;
  Boolean hasRequiredParameters = (colsParam == null || rowsParam == null) ? false : true;
  Boolean allParametersValid = false;

  if (hasRequiredParameters)
  {
    try
    {
      cols = Integer.parseInt(colsParam);
      rows = Integer.parseInt(rowsParam);
    }
    catch (Exception e)
    {
      allParametersValid = false;
    }
    
    allParametersValid = (cols > 0 && rows > 0) ? true : false;
  }
%>
<!DOCTYPE html>
<html lang="ru">
<head>
  <title></title>
  <meta charset="UTF-8">
  <link rel="stylesheet" href="style.css">
</head>
<body>
  <div class="button-back">
    <a href="./">&larr; Назад</a>
  </div>
  <div class="title">4. Взаимодействие HTML/JSP-сервлет</div>
  <div class="desc">
    3) Генерация таблиц по переданным параметрам: заголовок, количество строк и столбцов, цвет фона.
  </div>
    
  <div>Введите размеры таблицы</div>
  <form action="" method="get">
    <div class="input-group">
      <label for="title">Заголовок:</label>
      <input name="title" type="text" id="title" value="<%= titleParam == null ? "" : titleParam %>">
    </div>
    <div class="input-group">
      <label for="cols">Столбцы *:</label>
      <input name="cols" type="number" min="1" id="cols" value="<%= colsParam == null ? "" : colsParam %>">
    </div>
    <div class="input-group">
      <label for="rows">Строки *:</label>
      <input name="rows" type="number" min="1" id="rows" value="<%= rowsParam == null ? "" : rowsParam %>">
    </div>
    <div class="input-group">
      <label for="bg-color">Цвет фона:</label>
      <input name="bg-color" type="text" id="bg-color" value="<%= bgColorParam == null ? "" : bgColorParam %>">
    </div>
    <button>Сгенерировать таблицу</button>
  </form>

  <% if (hasRequiredParameters) { %>
    <span><%= (titleParam == null) ? "" : titleParam %></span>
    <% if (allParametersValid) { %>
      <table style="<%= (bgColorParam == null) ? "" : "background: " + bgColorParam %>">
        <% for (int i = 0; i < rows; ++i) { %>
          <tr>
            <% for (int j = 0; j < cols; ++j) { %>
              <td>
                <%= i %>
                <%= j %>
              </td>
            <% } %>
          </tr>
        <% } %>
      </table>
    <% } else { %>
      <div class="error">Пожалуйста, введите правильные данные</div>
    <% } %>
  <% } %>
</body>
</html>