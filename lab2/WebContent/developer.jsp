<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ru">
<head>
  <title></title>
  <meta charset="UTF-8">
  <link rel="stylesheet" href="style.css">
</head>
<body>
  <div class="button-back">
    <a href="./">&larr; Назад</a>
  </div>
  <div class="title">
    3. Создать приложение, выводящее фамилию разработчика, дату и время получения задания, а также дату и время его выполнения.
  </div>

  <table>
    <thead>
      <tr>
        <th>Разработчик</th>
        <th>Дата и время получения задания</th>
        <th>Дата и время выполнения задания</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Ждан Д. Н.</td>
        <td>25.01.2017 15:57:54</td>
        <td>12.05.2017 18:52:03</td>
      </tr>
    </tbody>
  </table>
</body>
</html>