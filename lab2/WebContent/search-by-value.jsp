<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%
  String searchParam = request.getParameter("s");
  Boolean hasParam = searchParam == null ? false : true;

  HashMap map = new HashMap<String, String>();
  map.put("List", "неупорядоченная коллекция, в которой допустимы дублирующие значения");
  map.put("Set", "неупорядоченная коллекция, не содержащая повторяющихся элементов");
  map.put("Queue", "коллекция, предназначенная для хранения элементов в порядке, нужном для их обработки");
  map.put("ArrayList", "обычный массив, длина которого автоматически увеличивается при добавлении новых элементов");
  map.put("LinkedList", "структура данных, состоящая из узлов, каждый из которых содержит как собственно данные, так и  две ссылки («связки») на следующий и предыдущий узел списка");
  map.put("HashSet", "коллекция, не позволяющая хранить одинаковые объекты");
  map.put("TreeSet", "коллекция, которая хранит свои элементы в виде упорядоченного по значениям дерева");

  List<String> mapKeys = new ArrayList<String>();
  mapKeys.addAll(map.keySet());

  String searchResult = null;
  if (hasParam)
  {
    searchResult = (String) map.get(searchParam);
  }
%>
<!DOCTYPE html>
<html lang="ru">
<head>
  <title></title>
  <meta charset="UTF-8">
  <link rel="stylesheet" href="style.css">
</head>
<body>
  <div class="button-back">
    <a href="./">&larr; Назад</a>
  </div>
  <div class="title">4. Взаимодействие HTML/JSP-сервлет</div>
  <div class="desc">
    6) Поиск и (или) замена информации в коллекции по ключу (значению).
  </div>

  <div>
    <span>Коллекция</span>
    <table>
      <thead>
        <tr>
          <th>Ключ</th>
          <th>Значение</th>
        </tr>
      </thead>
      <tbody>
        <% for (int i = 0; i < mapKeys.size(); ++i) { %>
          <tr>
            <% for (int j = 0; j < 2; ++j) { %>
              <td>
                <%= (j == 0) ? mapKeys.get(i) : map.get(mapKeys.get(i)) %>
              </td>
            <% } %>
          </tr>
        <% } %>
      </tbody>
    </table>
  </div>

  <br>

  <form action="" method="get">
    <div class="input-group">
      <label for="s">Введите ключ для поиска:</label>
      <input name="s" type="text" id="s" value="<%= hasParam ? searchParam : "" %>">
    </div>
    <button>Найти</button>
  </form>

  <% if (hasParam) {%>
    <div>
      <span class="title">Результаты поиска</span>
      <div>
        <% if (searchResult != null) {%>
            <em><%= searchParam %></em> - <%= searchResult %>
        <% } else { %>
          <span>Ничего не найдено</span>
        <% } %>
      </div>
    </div>
  <% } %>
</body>
</html>