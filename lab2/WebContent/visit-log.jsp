<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.io.File" %>
<%@ page import="java.io.BufferedReader" %>
<%@ page import="java.io.IOException" %>
<%@ page import="java.nio.file.Files" %>
<%@ page import="java.nio.file.Paths" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="java.util.Date" %>
<%
  String jspPath = getServletContext().getRealPath("/");
  String fileName = jspPath + java.io.File.separator + "visit-log.txt";
  List<List<String>> list = new ArrayList<List<String>>();

  try (BufferedReader br = Files.newBufferedReader(Paths.get(fileName)))
  {
    list = br
      .lines()
      .map(
        line -> new ArrayList<String>(Arrays.asList(line.split(";")))
          .stream()
          .map(String::trim)
          .collect(Collectors.toList())
      )
      .collect(Collectors.toList());
  }
  catch (IOException e)
  {
    e.printStackTrace();
  }


  String searchParam = request.getParameter("s");
  Boolean hasParam = searchParam == null ? false : true;

  List<List<String>> searchResults = null;

  if (hasParam)
  {
    searchResults = list
      .stream()
      .filter(record -> (record.get(0).toLowerCase().contains(searchParam.toLowerCase()) || record.get(2).equals(searchParam)))
      .collect(Collectors.toList());
  }


%>
<!DOCTYPE html>
<html lang="ru">
<head>
  <title></title>
  <meta charset="UTF-8">
  <link rel="stylesheet" href="style.css">
</head>
<body>
  <div class="button-back">
    <a href="./">&larr; Назад</a>
  </div>
  <div class="title">5. Журнал посещений</div>
  <div class="desc">
    Таблица должна быть в текстовом виде и содержать фамилию и инициалы, адрес, дату посещения. <br>
    Поиск должен производиться по части фамилии или по дате посещения.<br>
    Результаты должны выводиться вместе с датой выполнения в JSP.
  </div>
  
  <div>
    <span class="title">Посещения</span>
    <table>
      <thead>
        <tr>
          <th>Фамилия И. О.</th>
          <th>Адрес</th>
          <th>Дата посещения</th>
        </tr>
      </thead>
      <tbody>
        <% for (int i = 0; i < list.size(); ++i) { %>
          <tr>
            <% for (int j = 0; j < list.get(i).size(); ++j) { %>
              <td>
                <%= list.get(i).get(j) %>
              </td>
            <% } %>
          </tr>
        <% } %>
      </tbody>
    </table>
  </div>

  <br>

  <form action="" method="get">
    <div class="input-group">
      <label for="s">Введите часть фамилии или дату посещения:</label>
      <input name="s" type="text" id="s" value="<%= hasParam ? searchParam : "" %>">
    </div>
    <button>Найти</button>
  </form>

  <br>
  
  <% if (hasParam) {%>
    <div>
      <em>Дата выполнения в JSP:</em> <%= new Date() %>
    </div>
    <br>
    <div>
      <span class="title">Результаты поиска</span>
      <% if (searchResults != null) {%>
        <table>
          <thead>
            <tr>
              <th>Фамилия И. О.</th>
              <th>Адрес</th>
              <th>Дата посещения</th>
            </tr>
          </thead>
          <tbody>
            <% for (int i = 0; i < searchResults.size(); ++i) { %>
              <tr>
                <% for (int j = 0; j < searchResults.get(i).size(); ++j) { %>
                  <td>
                    <%= searchResults.get(i).get(j) %>
                  </td>
                <% } %>
              </tr>
            <% } %>
          </tbody>
        </table>
      <% } else { %>
        <span>Ничего не найдено</span>
      <% } %>
    </div>
  <% } %>
</body>
</html>