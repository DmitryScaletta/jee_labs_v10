<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
  String temperatureParam = request.getParameter("t");
  Boolean hasParam = temperatureParam == null ? false : true;
  Boolean isValidValue = true;
  
  Double temperature = null;
  String className = "";
  if (hasParam)
  {
    try
    {
      temperature = Double.parseDouble(temperatureParam);
    }
    catch (Exception e) {
       isValidValue = false;
    }
    
    if (isValidValue && temperature != 0)
    {
      className = (temperature > 0) ? "red" : "blue";
    }
  }
%>
<!DOCTYPE html>
<html lang="ru">
<head>
  <title></title>
  <meta charset="UTF-8">
  <link rel="stylesheet" href="style.css">
</head>
<body>
  <div class="button-back">
    <a href="./">&larr; Назад</a>
  </div>
  <div class="title">
    2. Задать температуру. Если она меньше нуля, вывести значение температуры синим цветом, если больше, то красным
  </div>

  <form action="" method="get">
    <div class="input-group">
      <label for="t">Введите температуру:</label>
      <input name="t" type="text" id="t" value="<%= hasParam ? temperatureParam : "" %>">
    </div>
    <button>Отправить</button>
  </form>

  <% if (hasParam) { %>
    <div class="temperature">
      <% if (isValidValue) { %>
        Температура:
        <span class="<%= className %>"><%= temperatureParam %></span>
      <% } else { %>
        <div class="error">Пожалуйста, введите правильное значение</div>
      <% } %>
    </div>
  <% } %>
</body>
</html>
